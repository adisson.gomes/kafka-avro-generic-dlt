package events.service.config

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig
import org.apache.avro.specific.SpecificRecordBase
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.ByteArraySerializer
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.kafka.config.TopicBuilder
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory

@Configuration
class KafkaConfiguration {

    @Bean
    fun eventTopic(@Value("\${topics.events}") topicName: String): NewTopic = TopicBuilder
        .name(topicName)
        .partitions(1)
        .replicas(1)
        .build()

    @Bean
    @Primary
    fun producerFactory(kafkaProperties: KafkaProperties): ProducerFactory<String, SpecificRecordBase> =
        DefaultKafkaProducerFactory(kafkaProperties.buildProducerProperties())

    @Bean
    fun dltProducerFactory(kafkaProperties: KafkaProperties): ProducerFactory<String, ByteArray> {
        val properties = kafkaProperties.buildProducerProperties()
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer::class.java)
        properties.put(KafkaAvroSerializerConfig.AUTO_REGISTER_SCHEMAS, false)
        return DefaultKafkaProducerFactory(properties)
    }

    @Bean
    @Primary
    fun kafkaTemplate(@Qualifier("producerFactory") producerFactory: ProducerFactory<String, SpecificRecordBase>) =
        KafkaTemplate(producerFactory)

    @Bean
    fun dltKafkaTemplate(@Qualifier("dltProducerFactory") dltProducerFactory: ProducerFactory<String, ByteArray>) =
        KafkaTemplate(dltProducerFactory)

    @Bean
    fun schemaRegistryClient(kafkaProperties: KafkaProperties): SchemaRegistryClient {
        val url = kafkaProperties.properties[KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG]
        return CachedSchemaRegistryClient(url, 1000)
    }
}
