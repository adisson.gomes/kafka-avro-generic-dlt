package events.service.config

import io.confluent.kafka.schemaregistry.avro.AvroSchema
import io.confluent.kafka.schemaregistry.avro.AvroSchemaUtils
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import org.apache.avro.generic.GenericData
import org.apache.avro.generic.GenericDatumWriter
import org.apache.avro.generic.GenericRecord
import org.apache.avro.io.EncoderFactory
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.Headers
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.listener.KafkaListenerErrorHandler
import org.springframework.kafka.listener.ListenerExecutionFailedException
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.Message
import org.springframework.stereotype.Component
import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer

@Component
class AppKafkaErrorHandler(
    @Value("\${topics.dlt}") private val dltTopic: String,
    @Value("\${spring.kafka.consumer.group-id}") private val groupId: String,
    @Qualifier("dltKafkaTemplate") private val kafkaTemplate: KafkaTemplate<String, ByteArray>,
    private val schemaRegistryClient: SchemaRegistryClient,
) : KafkaListenerErrorHandler {

    override fun handleError(message: Message<*>, exception: ListenerExecutionFailedException): Any {
        logger.warn("Message processing failed", exception)
        val consumerRecord = message.payload as ConsumerRecord<*, *>
        val key = consumerRecord.key() as String
        val value = consumerRecord.value() as GenericData.Record
        val schemaId = getSchemaId(consumerRecord)
        val outputStream = buildByteArray(value, schemaId)
        val record = ProducerRecord(dltTopic, key, outputStream.toByteArray())
        record.headers().prepareHeaders(consumerRecord)

        kafkaTemplate.send(record)

        return message
    }

    private fun getSchemaId(consumerRecord: ConsumerRecord<*, *>): Int {
        val value = consumerRecord.value() as GenericData.Record
        val topic = consumerRecord.topic()
        val schema = AvroSchema(AvroSchemaUtils.getSchema(value, false, false, false))
        return schemaRegistryClient.getId("$topic-value", schema, false)
    }

    private fun buildByteArray(payload: GenericData.Record, schemaId: Int): ByteArrayOutputStream {
        val outputStream = ByteArrayOutputStream()
        outputStream.write(MAGIC_BYTE)
        outputStream.write(ByteBuffer.allocate(ID_SIZE).putInt(schemaId).array())
        val encoder = EncoderFactory.get().binaryEncoder(outputStream, null)
        val writer = GenericDatumWriter<GenericRecord>(payload.schema)
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        out.write(MAGIC_BYTE);
//        out.write(ByteBuffer.allocate(idSize).putInt(id).array());
        writer.write(payload, encoder)
        encoder.flush()
        return outputStream
    }

    private fun Headers.prepareHeaders(consumerRecord: ConsumerRecord<*, *>) {
        for (header in consumerRecord.headers()) {
            when (header.key()) {
                DLT_RETRY_COUNT -> add(DLT_RETRY_COUNT, header.value())
            }
        }
        add(KafkaHeaders.DLT_ORIGINAL_TOPIC, consumerRecord.topic().toByteArray())
        add(KafkaHeaders.DLT_ORIGINAL_PARTITION, consumerRecord.partition().toByteArray())
        add(KafkaHeaders.DLT_ORIGINAL_CONSUMER_GROUP, groupId.toByteArray())
    }

    private fun Int.toByteArray() = this.toString().toByteArray()

    companion object {
        private const val DLT_RETRY_COUNT = "kafka_dlt-retry-count"
        private const val MAGIC_BYTE = 0x0
        private const val ID_SIZE = 4
        private val logger = LoggerFactory.getLogger(AppKafkaErrorHandler::class.java)
    }
}
