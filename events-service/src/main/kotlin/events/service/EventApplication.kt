package events.service

import events.service.producer.EventProducer
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@EnableKafka
class EventApplication

fun main(args: Array<String>) {
    runApplication<EventApplication>(*args)
}

@RestController
class TriggerController(private val eventProducer: EventProducer) {
    @PostMapping("/trigger")
    fun trigger() {
        val event = RandomStringUtils.randomAlphabetic(4, 10)
        eventProducer.publish("Event $event")
    }
}
