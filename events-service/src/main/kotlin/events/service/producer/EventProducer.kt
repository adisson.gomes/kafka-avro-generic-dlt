package events.service.producer

import com.adissongomes.events.AvroEvent
import org.apache.avro.specific.SpecificRecordBase
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.UUID

@Component
class EventProducer(
    @Qualifier("kafkaTemplate") private val kafkaTemplate: KafkaTemplate<String, SpecificRecordBase>,
    @Value("\${topics.events}") private val eventsTopic: String,
) {

    fun publish(event: String) {
        val avroEvent = AvroEvent(event, Instant.now())
        LoggerFactory.getLogger(EventProducer::class.java).info("Trying to publish $event to $eventsTopic")
        kafkaTemplate.send(eventsTopic, UUID.randomUUID().toString(), avroEvent)
    }
}
