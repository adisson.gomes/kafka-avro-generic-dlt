package events.service.consumer

import com.adissongomes.events.AvroEvent
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class EventConsumer {

    @KafkaListener(topics = ["\${topics.events}"], errorHandler = "appKafkaErrorHandler")
    fun handle(message: ConsumerRecord<String, AvroEvent>) {
        val mappedHeaders = message.headers().associate { Pair(it.key(), it.value().let { String(it) }) }

        log.info("New event received ${message.key()} - $mappedHeaders")

        throw RuntimeException("Forcing error")
    }

    companion object {
        private val log = LoggerFactory.getLogger(EventConsumer::class.java)
    }
}
