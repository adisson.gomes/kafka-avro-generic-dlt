plugins {
    id("org.springframework.boot")
    id("com.github.davidmc24.gradle.plugin.avro") version "1.8.0"
}

dependencies {
    implementation("org.springframework.kafka:spring-kafka")
    implementation("io.confluent:kafka-avro-serializer:7.0.1")
    implementation("org.apache.avro:avro:1.11.2")
    testImplementation("org.springframework.kafka:spring-kafka-test")
}
