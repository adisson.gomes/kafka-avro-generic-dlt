package kafkauniquedlt.kafka

import kafkauniquedlt.db.DltMessageRepository
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.Headers
import org.apache.kafka.common.header.internals.RecordHeader
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.stereotype.Component

@Component
class DltTopicConsumer(
    private val dltMessageRepository: DltMessageRepository,
    private val kafkaTemplate: KafkaTemplate<String, ByteArray>,
) {

    @KafkaListener(topics = ["\${topics.dlt-name}"])
    fun handle(message: ConsumerRecord<String, ByteArray>) {
        dltMessageRepository.add(message.value())
        forward(message)
    }

    private fun forward(message: ConsumerRecord<String, ByteArray>) {
        var retryCount = message.headers().lastHeaderAsInt(DLT_RETRY_COUNT)
        if (retryCount >= MAX_RETRIES) {
            log.warn("Message ${message.key()} has reached the max retries")
            return
        }
        retryCount++
        val originalTopic = message.headers().lastHeaderAsString(KafkaHeaders.DLT_ORIGINAL_TOPIC)
        val originalPartition = message.headers().lastHeaderAsInt(KafkaHeaders.DLT_ORIGINAL_PARTITION)
        val originConsumerGroup = message.headers().lastHeaderAsString(KafkaHeaders.DLT_ORIGINAL_CONSUMER_GROUP)

        log.info("Resend the message ${message.key()} to $originalTopic and consumer $originConsumerGroup")

        val headers = message.headers().toMutableSet().also {
            it.add(RecordHeader(DLT_RETRY_COUNT, retryCount.toString().toByteArray()))
        }

        val record = ProducerRecord(originalTopic, originalPartition, message.key(), message.value(), headers)
        kafkaTemplate.send(record)
    }

    companion object {
        private const val DLT_RETRY_COUNT = "kafka_dlt-retry-count"
        private const val MAX_RETRIES = 3
        private val log = LoggerFactory.getLogger(DltTopicConsumer::class.java)
    }
}

private fun Headers.lastHeaderAsString(name: String): String = lastHeader(name)?.let { String(it.value()) } ?: ""

private fun Headers.lastHeaderAsInt(name: String): Int = try {
    lastHeaderAsString(name).toInt()
} catch (e: NumberFormatException) {
    0
}
