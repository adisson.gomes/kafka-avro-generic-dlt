package kafkauniquedlt.kafka.config

import org.apache.kafka.clients.admin.NewTopic
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.TopicBuilder

@Configuration
class TopicsConfiguration {

    @Bean
    fun dltTopic(@Value("\${topics.dlt-name}") dltTopicName: String): NewTopic = TopicBuilder
        .name(dltTopicName)
        .partitions(1)
        .replicas(1)
        .build()
}
