package kafkauniquedlt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.kafka.annotation.EnableKafka

@SpringBootApplication
@EnableKafka
class KafkaUniqueDltApplication

fun main(args: Array<String>) {
    runApplication<KafkaUniqueDltApplication>(*args)
}
