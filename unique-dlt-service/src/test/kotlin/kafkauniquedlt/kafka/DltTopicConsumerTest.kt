package kafkauniquedlt.kafka

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.verify
import kafkauniquedlt.db.DltMessageRepository
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.internals.RecordHeader
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.test.context.TestPropertySource

@SpringBootTest
@EnableKafka
@EmbeddedKafka(
    bootstrapServersProperty = "spring.kafka.bootstrap-servers"
)
@TestPropertySource(properties = ["spring.kafka.consumer.auto-offset-reset=earliest"])
class DltTopicConsumerTest {

    @Autowired
    private lateinit var template: KafkaTemplate<String, ByteArray>

    @MockkBean
    private lateinit var repository: DltMessageRepository
    private val mapper = jacksonObjectMapper()

    @Test
    fun consumer() {
        val byteArray = mapper.writeValueAsBytes(mapOf("key" to "any value"))
        val headersList = listOf(RecordHeader("origin", "some-topic".toByteArray()))
        val record = ProducerRecord("general.dlt", null, "1", byteArray, headersList)

        template.send(record)

        verify(timeout = 3000) { repository.add(any()) }
    }
}
